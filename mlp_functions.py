""" Projet d'année partie 2 : Fonctions"""

__author__ = "Noemie Muller"
__matricule__ = "000458865"
__date__ = "12/2017"
__cours__ = "info_f-106"
__titulaire__ = "?"

import numpy as np

def readData(filename):
    """
    Ouvre le fichier filename et convertir les données en matrice.

    """
    f = open(filename)
    L = []
    for line in f:
        elem = [int(s) for s in line.strip().split(',')]
        L += [elem]
    return np.array(L)

def initWeights(nb_rows,nb_columns) :
    """
    Initialise aléatoirement une matrice de poids W de taille nb_rows x nb_columns.

    """
    W = np.random.normal(0,0.0001,(nb_rows,nb_columns))
    return W

def convergenceCondition(DeltaW1,DeltaW2,epsilon=0.0001) :
    """
    Vérifie l'amélioration des matrices de poids W1 et W2.

    """
    try :
        res = np.sum(abs(DeltaW1)) < epsilon and np.sum(abs(DeltaW2)) < epsilon
    except TypeError :
        res = False
    return res

def dropout(W1,p) :
    """
    Implémente la procédure de dropout avec une probabilité de sélection p.

    """
    nb_middle, nb_input = np.shape(W1)
    D_input = np.random.choice([0,1],nb_input, p = [p,1-p])
    D_middle = np.random.choice([0,1],(nb_middle,1), p = [p,1-p])
    return (D_input,D_middle)

def checkDigit(a, digit) :
    return 1 if a==digit else -1

def sigm(x) :
    return 1/(1+np.exp(-x))

def derivee(x) :
    return (1 - x)*x

def forwardPass(x,W1,W2) :
    """
    Effecture une prédicition sur l'input x en calculant les entrées et
    sorties de chaque neurone.

    """
    s = W1@x #entrées des neurones intermédiaires (vecteur)
    z = sigm(s) #activation de chaque neurone intermédiaire
    grad = derivee(z)
    s_out = z@W2 #entrée du neurone de sortie
    z_out = sigm(s_out) #activation du neurone de sortie
    grad_out = derivee(z_out) #dérivée de l'activation du neurone de sortie

    return (z,z_out,grad,grad_out)

def determinationPredict(z_out) :
    return 1 if z_out >= 0.5 else -1

def backpropagation(x,y,y_hat,z,W1,W2,grad,grad_out) :
    """
    Effectue la backpropagation d'un perceptron multicouche.

    """
    e_out = y_hat - y #nul si bonne prédiction
    delta_out = e_out*grad_out
    delta_i = delta_out*np.row_stack(grad)*W2
    DeltaW1 = delta_i*x
    DeltaW2 = delta_out* np.row_stack(z)
    return (DeltaW1,DeltaW2)

def evaluate(data, W1, W2, digit) :
    """
    Evalue la performance des matrices poids W1 et W2 pour reconnaitre le digit.

    """
    predict_ok = 0
    for i in data :
        x = i[1:]
        y = checkDigit(i[0],digit)
        z_out = forwardPass(x,W1,W2)[1]
        if z_out >= 0.5 :
            y_hat = 1
        else :
            y_hat = -1
        if y_hat == y :
                predict_ok += 1
    return predict_ok

def train(data,digit,p=0.5,epsilon=0.0001,H=10,learning_rate=0.001,max_it=20) : 
    """
    Entraine un perceptron sur le training set data pour reconnaitre le chiffre digit.
    Renvoie les meilleures matrices poids W1 et W2 trouvées.

    """
    W1 = initWeights(H,(len(data[0])-1)) #initilisation des deux matrices de poids
    W2 = initWeights(H,1)
    np.random.shuffle(data)
    it = 0
    meilleur_score = 0
    DeltaW1,DeltaW2 = None,None
    data_copy = np.copy(data)
    perfect_training = False
    while not convergenceCondition(DeltaW1,DeltaW2,epsilon=0.0001) and it < max_it and not perfect_training :
        D_input,D_middle = dropout(W1,p)
        while not 1 in D_input or not 1 in D_middle :
            D_input,D_middle = dropout(W1,p)
        W1_copy = W1*D_input
        W2_copy = W2*D_middle
        for line in data_copy : #training round
            x = line[1:]
            y = checkDigit(line[0],digit)
            z,z_out,grad,grad_out = forwardPass(x,W1_copy,W2_copy)
            y_hat = determinationPredict(z_out)
            if y != y_hat :
                DeltaW1,DeltaW2 = backpropagation(x,y,y_hat,z,W1_copy,W2_copy,grad,grad_out)
                W1 = W1 - learning_rate*DeltaW1
                W2 = W2 - learning_rate*DeltaW2
        score = evaluate(data_copy, W1, W2, digit)
        if score >= meilleur_score :
            meilleur_score = score #on retient le meilleur résultat
            best_W1 = np.copy(W1)
            best_W2 = np.copy(W2) #on retient les matrices poids avec les meilleures performances
        perfect_training = meilleur_score is len(data_copy)
        W1, W2 = np.copy(best_W1),np.copy(best_W2)
        it += 1
    return (W1,W2)

def trainPerceptrons(data,p=0.5,epsilon=0.0001,H=10,learning_rate=0.001,max_it=20):
    L = [] 
    for digit in range(10):       
        print("Training perceptron {}".format(digit))
        L.append(train(data,digit,p,epsilon,H,learning_rate,max_it))
    return L

def predict(x,L) :
    """
    Effectue la prédiction (globale) du chiffre representé sur l'input x.

    """
    best_prediction = None
    for d in range(10):
        W1,W2 = L[d]
        z_out = forwardPass(x,W1,W2)[1] 
        if best_prediction==None or z_out > best_prediction:
            best_prediction = z_out
            digit = d
    return digit

def print_color(string,color,sep) :
    """
    Affiche la chaine de caractère 'string' dans la couleur de code 'color', 
    avec 'sep' pour séparateur.

    """
    print("\x1B[{0}m{1}\x1B[0m".format(color,string),end=sep)

def choice_color(value,bool,ColorDiagonale,ColorRest,inter) :
    for i in range(len(inter)) :
        if inter[i][0] <= value <= inter[i][1] :
            if bool :
                color = ColorRest[i]
            else :
                color = ColorDiagonale[i]
    return color

def displayLegende(ColorDiagonale,ColorRest,inter) :
    """
    Affiche la légende de la matrice de confusion.

    """
    ColorDiagonale = ['38;5;227','38;5;191','38;5;115','38;5;119','38;5;83']
    ColorRest = ['38;5;227','38;5;221','38;5;215','38;5;209','38;5;203']
    inter = [(0,40),(40,50),(50,60),(60,80),(80,100)]
    for i in range(len(inter)) :
        print_color('Pourcentages de prédictions correctes/',ColorRest[i],'')
        print_color('incorrectes',ColorDiagonale[i],' ')
        intervalle = inter[i]
        print("dans l'intervalle [{0},{1}[.".format(inter[i][0],inter[i][1]))

def displayConfusionMatrix(C) :
    """
    Affiche la matrice de confusion C.

    """
    ColorDiagonale = ['38;5;227','38;5;191','38;5;115','38;5;119','38;5;83']
    ColorRest = ['38;5;227','38;5;221','38;5;215','38;5;209','38;5;203']
    inter = [(0,40),(40,50),(50,60),(60,80),(80,100)]
    for i in range(10) :
        for j in range(10) :
            bool = i!=j
            color = choice_color(C[i][j],bool,ColorDiagonale,ColorRest,inter)
            nb = str(C[i][j])
            sep = (6-len(nb))*' '
            print_color(nb+'%',color,sep)
        print('\n',end='')
    print()
    displayLegende(ColorDiagonale,ColorRest,inter)
    print()

def crossValidation(data,alpha=0.5,it_CV=4,p=0.5,epsilon=0.0001,H=10,learning_rate=0.001,max_it=10) :
    """
    Teste les performances du réseau.

    """
    print("starting cross validation ({} rounds)".format(it_CV))
    total = 0
    bound_training_set = int(len(data)*alpha)
    for i in range(it_CV) :  #on fait it_CV tests et on prend la moyenne des résultats
        print('round', i+1)
        np.random.shuffle(data)
        training_set = data[:bound_training_set]
        test_set = data[bound_training_set:]
        L = trainPerceptrons(training_set,p,epsilon,H,learning_rate,max_it)
        C = [[0 for i in range(10)]for j in range(10)]
        count_digit = [0 for i in range(10)]
        score = 0
        for i in test_set :
            x = i[1:]
            y = i[0]
            y_hat = predict(x,L)
            C[y][y_hat] += 1
            count_digit[y] += 1
            if y == y_hat : #on vérifie si la prédiction est correcte
                score += 1
        print('score:', score/len(test_set))
        total += score
        for i in range(10) :
            for j in range(10) :
                C[i][j] = round(C[i][j]/count_digit[i]*100,2)
        print()
        displayConfusionMatrix(C)
    score_moyen = total/it_CV
    pourcentage_moyen = score_moyen/len(test_set)*100
    print("alpha =",alpha," it_CV =",it_CV," p =",p," epsilon =",epsilon," H =",H,end=" ")
    print(" learning_rate =",learning_rate," max_it =",max_it)
    print("Taux de réussite moyen :",pourcentage_moyen,"%")