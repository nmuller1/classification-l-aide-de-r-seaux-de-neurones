""" Projet d'année partie 2 : Boucle principale """

__author__ = "Noemie Muller"
__matricule__ = "000458865"
__date__ = "12/2017"
__cours__ = "info_f-106"
__titulaire__ = "?"

from mlp_functions import crossValidation,readData

if __name__ == '__main__':
    crossValidation(readData("train_small.csv"))